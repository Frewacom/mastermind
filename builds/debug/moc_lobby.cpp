/****************************************************************************
** Meta object code from reading C++ file 'lobby.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../source/lobby.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'lobby.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Lobby_t {
    QByteArrayData data[35];
    char stringdata0[497];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Lobby_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Lobby_t qt_meta_stringdata_Lobby = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Lobby"
QT_MOC_LITERAL(1, 6, 12), // "gameFinished"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 22), // "on_PlayOffline_clicked"
QT_MOC_LITERAL(4, 43, 23), // "on_CreateServer_clicked"
QT_MOC_LITERAL(5, 67, 26), // "on_ConnectToServer_clicked"
QT_MOC_LITERAL(6, 94, 20), // "on_InputSend_clicked"
QT_MOC_LITERAL(7, 115, 28), // "on_ClearOutputButton_clicked"
QT_MOC_LITERAL(8, 144, 21), // "connectionEstablished"
QT_MOC_LITERAL(9, 166, 13), // "userConnected"
QT_MOC_LITERAL(10, 180, 25), // "maximumConnectionsReached"
QT_MOC_LITERAL(11, 206, 20), // "recievedPlayersReady"
QT_MOC_LITERAL(12, 227, 4), // "data"
QT_MOC_LITERAL(13, 232, 19), // "recievedChatMessage"
QT_MOC_LITERAL(14, 252, 3), // "msg"
QT_MOC_LITERAL(15, 256, 15), // "recievedAttempt"
QT_MOC_LITERAL(16, 272, 12), // "recievedCode"
QT_MOC_LITERAL(17, 285, 17), // "recievedPlayerWon"
QT_MOC_LITERAL(18, 303, 24), // "recievedConnectionDenied"
QT_MOC_LITERAL(19, 328, 16), // "userDisconnected"
QT_MOC_LITERAL(20, 345, 15), // "youDisconnected"
QT_MOC_LITERAL(21, 361, 11), // "wroteToChat"
QT_MOC_LITERAL(22, 373, 10), // "selectCode"
QT_MOC_LITERAL(23, 384, 7), // "codeSet"
QT_MOC_LITERAL(24, 392, 5), // "Code*"
QT_MOC_LITERAL(25, 398, 4), // "code"
QT_MOC_LITERAL(26, 403, 11), // "attemptMade"
QT_MOC_LITERAL(27, 415, 8), // "Response"
QT_MOC_LITERAL(28, 424, 4), // "resp"
QT_MOC_LITERAL(29, 429, 15), // "playerFoundCode"
QT_MOC_LITERAL(30, 445, 11), // "errorHandle"
QT_MOC_LITERAL(31, 457, 10), // "IPAccepted"
QT_MOC_LITERAL(32, 468, 2), // "ip"
QT_MOC_LITERAL(33, 471, 8), // "IPDenied"
QT_MOC_LITERAL(34, 480, 16) // "hostnameSelected"

    },
    "Lobby\0gameFinished\0\0on_PlayOffline_clicked\0"
    "on_CreateServer_clicked\0"
    "on_ConnectToServer_clicked\0"
    "on_InputSend_clicked\0on_ClearOutputButton_clicked\0"
    "connectionEstablished\0userConnected\0"
    "maximumConnectionsReached\0"
    "recievedPlayersReady\0data\0recievedChatMessage\0"
    "msg\0recievedAttempt\0recievedCode\0"
    "recievedPlayerWon\0recievedConnectionDenied\0"
    "userDisconnected\0youDisconnected\0"
    "wroteToChat\0selectCode\0codeSet\0Code*\0"
    "code\0attemptMade\0Response\0resp\0"
    "playerFoundCode\0errorHandle\0IPAccepted\0"
    "ip\0IPDenied\0hostnameSelected"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Lobby[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  144,    2, 0x08 /* Private */,
       3,    0,  145,    2, 0x08 /* Private */,
       4,    0,  146,    2, 0x08 /* Private */,
       5,    0,  147,    2, 0x08 /* Private */,
       6,    0,  148,    2, 0x08 /* Private */,
       7,    0,  149,    2, 0x08 /* Private */,
       8,    0,  150,    2, 0x08 /* Private */,
       9,    0,  151,    2, 0x08 /* Private */,
      10,    0,  152,    2, 0x08 /* Private */,
      11,    1,  153,    2, 0x08 /* Private */,
      13,    1,  156,    2, 0x08 /* Private */,
      15,    1,  159,    2, 0x08 /* Private */,
      16,    1,  162,    2, 0x08 /* Private */,
      17,    1,  165,    2, 0x08 /* Private */,
      18,    0,  168,    2, 0x08 /* Private */,
      19,    0,  169,    2, 0x08 /* Private */,
      20,    0,  170,    2, 0x08 /* Private */,
      21,    1,  171,    2, 0x08 /* Private */,
      22,    0,  174,    2, 0x08 /* Private */,
      23,    1,  175,    2, 0x08 /* Private */,
      26,    2,  178,    2, 0x08 /* Private */,
      29,    0,  183,    2, 0x08 /* Private */,
      30,    1,  184,    2, 0x08 /* Private */,
      31,    1,  187,    2, 0x08 /* Private */,
      33,    0,  190,    2, 0x08 /* Private */,
      34,    1,  191,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void, QMetaType::QString,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void, 0x80000000 | 24, 0x80000000 | 27,   25,   28,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   14,
    QMetaType::Void, QMetaType::QString,   32,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   32,

       0        // eod
};

void Lobby::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Lobby *_t = static_cast<Lobby *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->gameFinished(); break;
        case 1: _t->on_PlayOffline_clicked(); break;
        case 2: _t->on_CreateServer_clicked(); break;
        case 3: _t->on_ConnectToServer_clicked(); break;
        case 4: _t->on_InputSend_clicked(); break;
        case 5: _t->on_ClearOutputButton_clicked(); break;
        case 6: _t->connectionEstablished(); break;
        case 7: _t->userConnected(); break;
        case 8: _t->maximumConnectionsReached(); break;
        case 9: _t->recievedPlayersReady((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->recievedChatMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->recievedAttempt((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 12: _t->recievedCode((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: _t->recievedPlayerWon((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 14: _t->recievedConnectionDenied(); break;
        case 15: _t->userDisconnected(); break;
        case 16: _t->youDisconnected(); break;
        case 17: _t->wroteToChat((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 18: _t->selectCode(); break;
        case 19: _t->codeSet((*reinterpret_cast< Code*(*)>(_a[1]))); break;
        case 20: _t->attemptMade((*reinterpret_cast< Code*(*)>(_a[1])),(*reinterpret_cast< Response(*)>(_a[2]))); break;
        case 21: _t->playerFoundCode(); break;
        case 22: _t->errorHandle((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 23: _t->IPAccepted((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 24: _t->IPDenied(); break;
        case 25: _t->hostnameSelected((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject Lobby::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Lobby.data,
      qt_meta_data_Lobby,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Lobby::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Lobby::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Lobby.stringdata0))
        return static_cast<void*>(const_cast< Lobby*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Lobby::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
