/********************************************************************************
** Form generated from reading UI file 'customtextdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CUSTOMTEXTDIALOG_H
#define UI_CUSTOMTEXTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_CustomTextDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *MainVLayout;
    QLabel *CustomLabel;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CustomTextDialog)
    {
        if (CustomTextDialog->objectName().isEmpty())
            CustomTextDialog->setObjectName(QStringLiteral("CustomTextDialog"));
        CustomTextDialog->resize(315, 63);
        gridLayout = new QGridLayout(CustomTextDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        MainVLayout = new QVBoxLayout();
        MainVLayout->setObjectName(QStringLiteral("MainVLayout"));
        CustomLabel = new QLabel(CustomTextDialog);
        CustomLabel->setObjectName(QStringLiteral("CustomLabel"));
        CustomLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        MainVLayout->addWidget(CustomLabel);


        gridLayout->addLayout(MainVLayout, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(CustomTextDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);


        retranslateUi(CustomTextDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), CustomTextDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), CustomTextDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(CustomTextDialog);
    } // setupUi

    void retranslateUi(QDialog *CustomTextDialog)
    {
        CustomTextDialog->setWindowTitle(QApplication::translate("CustomTextDialog", "Du vann!", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        CustomTextDialog->setAccessibleName(QApplication::translate("CustomTextDialog", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        CustomLabel->setAccessibleName(QApplication::translate("CustomTextDialog", "CustomLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        CustomLabel->setText(QApplication::translate("CustomTextDialog", "TextLabel", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class CustomTextDialog: public Ui_CustomTextDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CUSTOMTEXTDIALOG_H
