/********************************************************************************
** Form generated from reading UI file 'gameoverdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMEOVERDIALOG_H
#define UI_GAMEOVERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_GameOverDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *MainVLayout;
    QLabel *GameOverLabel;
    QHBoxLayout *ColorBoxHLayout;
    QFrame *ColorBox1;
    QFrame *ColorBox2;
    QFrame *ColorBox3;
    QFrame *ColorBox4;
    QDialogButtonBox *ControlsBox;

    void setupUi(QDialog *GameOverDialog)
    {
        if (GameOverDialog->objectName().isEmpty())
            GameOverDialog->setObjectName(QStringLiteral("GameOverDialog"));
        GameOverDialog->resize(400, 161);
        gridLayout = new QGridLayout(GameOverDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        MainVLayout = new QVBoxLayout();
        MainVLayout->setObjectName(QStringLiteral("MainVLayout"));
        GameOverLabel = new QLabel(GameOverDialog);
        GameOverLabel->setObjectName(QStringLiteral("GameOverLabel"));
        GameOverLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        MainVLayout->addWidget(GameOverLabel);

        ColorBoxHLayout = new QHBoxLayout();
        ColorBoxHLayout->setObjectName(QStringLiteral("ColorBoxHLayout"));
        ColorBox1 = new QFrame(GameOverDialog);
        ColorBox1->setObjectName(QStringLiteral("ColorBox1"));
        ColorBox1->setFrameShape(QFrame::StyledPanel);
        ColorBox1->setFrameShadow(QFrame::Raised);

        ColorBoxHLayout->addWidget(ColorBox1);

        ColorBox2 = new QFrame(GameOverDialog);
        ColorBox2->setObjectName(QStringLiteral("ColorBox2"));
        ColorBox2->setFrameShape(QFrame::StyledPanel);
        ColorBox2->setFrameShadow(QFrame::Raised);

        ColorBoxHLayout->addWidget(ColorBox2);

        ColorBox3 = new QFrame(GameOverDialog);
        ColorBox3->setObjectName(QStringLiteral("ColorBox3"));
        ColorBox3->setFrameShape(QFrame::StyledPanel);
        ColorBox3->setFrameShadow(QFrame::Raised);

        ColorBoxHLayout->addWidget(ColorBox3);

        ColorBox4 = new QFrame(GameOverDialog);
        ColorBox4->setObjectName(QStringLiteral("ColorBox4"));
        ColorBox4->setFrameShape(QFrame::StyledPanel);
        ColorBox4->setFrameShadow(QFrame::Raised);

        ColorBoxHLayout->addWidget(ColorBox4);


        MainVLayout->addLayout(ColorBoxHLayout);

        MainVLayout->setStretch(0, 1);
        MainVLayout->setStretch(1, 3);

        gridLayout->addLayout(MainVLayout, 0, 0, 1, 1);

        ControlsBox = new QDialogButtonBox(GameOverDialog);
        ControlsBox->setObjectName(QStringLiteral("ControlsBox"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ControlsBox->sizePolicy().hasHeightForWidth());
        ControlsBox->setSizePolicy(sizePolicy);
        ControlsBox->setLayoutDirection(Qt::LeftToRight);
        ControlsBox->setOrientation(Qt::Horizontal);
        ControlsBox->setStandardButtons(QDialogButtonBox::Ok);

        gridLayout->addWidget(ControlsBox, 1, 0, 1, 1);


        retranslateUi(GameOverDialog);
        QObject::connect(ControlsBox, SIGNAL(accepted()), GameOverDialog, SLOT(accept()));
        QObject::connect(ControlsBox, SIGNAL(rejected()), GameOverDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(GameOverDialog);
    } // setupUi

    void retranslateUi(QDialog *GameOverDialog)
    {
        GameOverDialog->setWindowTitle(QApplication::translate("GameOverDialog", "Gameover!", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        GameOverDialog->setAccessibleName(QApplication::translate("GameOverDialog", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        GameOverLabel->setText(QApplication::translate("GameOverDialog", "Tyv\303\244rr, du f\303\266rlorade. Det h\303\244r var den r\303\244tta koden:", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ColorBox1->setAccessibleName(QApplication::translate("GameOverDialog", "GameOverBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ColorBox2->setAccessibleName(QApplication::translate("GameOverDialog", "GameOverBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ColorBox3->setAccessibleName(QApplication::translate("GameOverDialog", "GameOverBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ColorBox4->setAccessibleName(QApplication::translate("GameOverDialog", "GameOverBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ControlsBox->setAccessibleName(QApplication::translate("GameOverDialog", "ControlsBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
    } // retranslateUi

};

namespace Ui {
    class GameOverDialog: public Ui_GameOverDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMEOVERDIALOG_H
