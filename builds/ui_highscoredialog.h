/********************************************************************************
** Form generated from reading UI file 'highscoredialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HIGHSCOREDIALOG_H
#define UI_HIGHSCOREDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_HighscoreDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout;
    QLabel *InfoLabel;
    QHBoxLayout *horizontalLayout;
    QLabel *NameLabel;
    QLineEdit *NameInput;
    QLabel *ScoreLabel;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *HighscoreDialog)
    {
        if (HighscoreDialog->objectName().isEmpty())
            HighscoreDialog->setObjectName(QStringLiteral("HighscoreDialog"));
        HighscoreDialog->resize(297, 147);
        HighscoreDialog->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(HighscoreDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        InfoLabel = new QLabel(HighscoreDialog);
        InfoLabel->setObjectName(QStringLiteral("InfoLabel"));

        verticalLayout->addWidget(InfoLabel);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        NameLabel = new QLabel(HighscoreDialog);
        NameLabel->setObjectName(QStringLiteral("NameLabel"));

        horizontalLayout->addWidget(NameLabel);

        NameInput = new QLineEdit(HighscoreDialog);
        NameInput->setObjectName(QStringLiteral("NameInput"));

        horizontalLayout->addWidget(NameInput);


        verticalLayout->addLayout(horizontalLayout);

        ScoreLabel = new QLabel(HighscoreDialog);
        ScoreLabel->setObjectName(QStringLiteral("ScoreLabel"));

        verticalLayout->addWidget(ScoreLabel);

        buttonBox = new QDialogButtonBox(HighscoreDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);

        verticalLayout->setStretch(1, 2);
        verticalLayout->setStretch(3, 1);

        gridLayout->addLayout(verticalLayout, 0, 0, 1, 1);


        retranslateUi(HighscoreDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), HighscoreDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), HighscoreDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(HighscoreDialog);
    } // setupUi

    void retranslateUi(QDialog *HighscoreDialog)
    {
        HighscoreDialog->setWindowTitle(QApplication::translate("HighscoreDialog", "Highscore", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        HighscoreDialog->setAccessibleName(QApplication::translate("HighscoreDialog", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        InfoLabel->setText(QApplication::translate("HighscoreDialog", "Grattis, du vann och fick h\303\266gre po\303\244ng \303\244n n\303\245gonsin!\n"
"Skriv in ditt namn f\303\266r att spara ditt resultat:", Q_NULLPTR));
        NameLabel->setText(QApplication::translate("HighscoreDialog", "Namn:", Q_NULLPTR));
        ScoreLabel->setText(QApplication::translate("HighscoreDialog", "Po\303\244ng: 0", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class HighscoreDialog: public Ui_HighscoreDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HIGHSCOREDIALOG_H
