/********************************************************************************
** Form generated from reading UI file 'lobby.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOBBY_H
#define UI_LOBBY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Lobby
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *MainVLayout;
    QFrame *HeaderFrame;
    QHBoxLayout *horizontalLayout;
    QLabel *Title;
    QFrame *ControlsHFrame;
    QHBoxLayout *ControlsHLayout;
    QPushButton *ConnectToServer;
    QPushButton *PlayOffline;
    QPushButton *CreateServer;
    QVBoxLayout *OutputVLayout;
    QTextEdit *Output;
    QHBoxLayout *OutputControlsHLayout;
    QLineEdit *Input;
    QPushButton *InputSend;
    QPushButton *ClearOutputButton;
    QFrame *FooterFrame;
    QHBoxLayout *RulesHLayout;
    QHBoxLayout *FooterHLayout;
    QPushButton *RulesButton;
    QPushButton *ContactButton;
    QLabel *AboutLabel;

    void setupUi(QMainWindow *Lobby)
    {
        if (Lobby->objectName().isEmpty())
            Lobby->setObjectName(QStringLiteral("Lobby"));
        Lobby->resize(585, 550);
        centralWidget = new QWidget(Lobby);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(5);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        MainVLayout = new QVBoxLayout();
        MainVLayout->setSpacing(0);
        MainVLayout->setObjectName(QStringLiteral("MainVLayout"));
        HeaderFrame = new QFrame(centralWidget);
        HeaderFrame->setObjectName(QStringLiteral("HeaderFrame"));
        HeaderFrame->setFrameShape(QFrame::StyledPanel);
        HeaderFrame->setFrameShadow(QFrame::Raised);
        horizontalLayout = new QHBoxLayout(HeaderFrame);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        Title = new QLabel(HeaderFrame);
        Title->setObjectName(QStringLiteral("Title"));
        QFont font;
        font.setFamily(QStringLiteral("Lato Light"));
        font.setPointSize(36);
        font.setBold(false);
        font.setWeight(50);
        Title->setFont(font);
        Title->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(Title);


        MainVLayout->addWidget(HeaderFrame);

        ControlsHFrame = new QFrame(centralWidget);
        ControlsHFrame->setObjectName(QStringLiteral("ControlsHFrame"));
        ControlsHLayout = new QHBoxLayout(ControlsHFrame);
        ControlsHLayout->setSpacing(0);
        ControlsHLayout->setObjectName(QStringLiteral("ControlsHLayout"));
        ControlsHLayout->setContentsMargins(0, 0, 0, 0);
        ConnectToServer = new QPushButton(ControlsHFrame);
        ConnectToServer->setObjectName(QStringLiteral("ConnectToServer"));

        ControlsHLayout->addWidget(ConnectToServer);

        PlayOffline = new QPushButton(ControlsHFrame);
        PlayOffline->setObjectName(QStringLiteral("PlayOffline"));

        ControlsHLayout->addWidget(PlayOffline);

        CreateServer = new QPushButton(ControlsHFrame);
        CreateServer->setObjectName(QStringLiteral("CreateServer"));

        ControlsHLayout->addWidget(CreateServer);

        ControlsHLayout->setStretch(0, 4);
        ControlsHLayout->setStretch(1, 5);
        ControlsHLayout->setStretch(2, 4);

        MainVLayout->addWidget(ControlsHFrame);

        OutputVLayout = new QVBoxLayout();
        OutputVLayout->setSpacing(0);
        OutputVLayout->setObjectName(QStringLiteral("OutputVLayout"));
        OutputVLayout->setContentsMargins(10, 20, 10, 10);
        Output = new QTextEdit(centralWidget);
        Output->setObjectName(QStringLiteral("Output"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Output->sizePolicy().hasHeightForWidth());
        Output->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setFamily(QStringLiteral("Yu Gothic UI Semilight"));
        font1.setPointSize(12);
        Output->setFont(font1);
        Output->setFrameShape(QFrame::StyledPanel);
        Output->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        Output->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        Output->setReadOnly(true);

        OutputVLayout->addWidget(Output);

        OutputControlsHLayout = new QHBoxLayout();
        OutputControlsHLayout->setSpacing(0);
        OutputControlsHLayout->setObjectName(QStringLiteral("OutputControlsHLayout"));
        OutputControlsHLayout->setContentsMargins(10, -1, 10, -1);
        Input = new QLineEdit(centralWidget);
        Input->setObjectName(QStringLiteral("Input"));

        OutputControlsHLayout->addWidget(Input);

        InputSend = new QPushButton(centralWidget);
        InputSend->setObjectName(QStringLiteral("InputSend"));
        InputSend->setAutoDefault(true);

        OutputControlsHLayout->addWidget(InputSend);

        ClearOutputButton = new QPushButton(centralWidget);
        ClearOutputButton->setObjectName(QStringLiteral("ClearOutputButton"));

        OutputControlsHLayout->addWidget(ClearOutputButton);


        OutputVLayout->addLayout(OutputControlsHLayout);


        MainVLayout->addLayout(OutputVLayout);

        FooterFrame = new QFrame(centralWidget);
        FooterFrame->setObjectName(QStringLiteral("FooterFrame"));
        RulesHLayout = new QHBoxLayout(FooterFrame);
        RulesHLayout->setSpacing(5);
        RulesHLayout->setObjectName(QStringLiteral("RulesHLayout"));
        RulesHLayout->setContentsMargins(20, 0, 0, 0);
        FooterHLayout = new QHBoxLayout();
        FooterHLayout->setObjectName(QStringLiteral("FooterHLayout"));
        RulesButton = new QPushButton(FooterFrame);
        RulesButton->setObjectName(QStringLiteral("RulesButton"));

        FooterHLayout->addWidget(RulesButton);

        ContactButton = new QPushButton(FooterFrame);
        ContactButton->setObjectName(QStringLiteral("ContactButton"));

        FooterHLayout->addWidget(ContactButton);

        AboutLabel = new QLabel(FooterFrame);
        AboutLabel->setObjectName(QStringLiteral("AboutLabel"));
        AboutLabel->setScaledContents(false);
        AboutLabel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        AboutLabel->setWordWrap(true);
        AboutLabel->setMargin(0);

        FooterHLayout->addWidget(AboutLabel);

        FooterHLayout->setStretch(2, 1);

        RulesHLayout->addLayout(FooterHLayout);


        MainVLayout->addWidget(FooterFrame);

        MainVLayout->setStretch(0, 3);
        MainVLayout->setStretch(2, 6);

        gridLayout->addLayout(MainVLayout, 0, 0, 1, 1);

        Lobby->setCentralWidget(centralWidget);

        retranslateUi(Lobby);

        InputSend->setDefault(true);


        QMetaObject::connectSlotsByName(Lobby);
    } // setupUi

    void retranslateUi(QMainWindow *Lobby)
    {
        Lobby->setWindowTitle(QApplication::translate("Lobby", "Mastermind Lobby", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        Lobby->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        centralWidget->setAccessibleName(QApplication::translate("Lobby", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        HeaderFrame->setAccessibleName(QApplication::translate("Lobby", "Header", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        Title->setAccessibleName(QApplication::translate("Lobby", "Title", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        Title->setText(QApplication::translate("Lobby", "Mastermind", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ControlsHFrame->setAccessibleName(QApplication::translate("Lobby", "ControlsFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ConnectToServer->setAccessibleName(QApplication::translate("Lobby", "LobbyButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ConnectToServer->setText(QApplication::translate("Lobby", "Spela online", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        PlayOffline->setAccessibleName(QApplication::translate("Lobby", "LobbyButtonCenter", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        PlayOffline->setText(QApplication::translate("Lobby", "Spela sj\303\244lv", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        CreateServer->setAccessibleName(QApplication::translate("Lobby", "LobbyButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        CreateServer->setText(QApplication::translate("Lobby", "Skapa server", Q_NULLPTR));
        Output->setHtml(QApplication::translate("Lobby", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Yu Gothic UI Semilight'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Fixedsys';\"><br /></p></body></html>", Q_NULLPTR));
        Input->setPlaceholderText(QApplication::translate("Lobby", "Skriv n\303\245got..", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        InputSend->setAccessibleName(QApplication::translate("Lobby", "OutputButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        InputSend->setText(QApplication::translate("Lobby", "Skicka", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ClearOutputButton->setAccessibleName(QApplication::translate("Lobby", "OutputButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ClearOutputButton->setText(QApplication::translate("Lobby", "Rensa", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        FooterFrame->setAccessibleName(QApplication::translate("Lobby", "FooterFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        RulesButton->setAccessibleName(QApplication::translate("Lobby", "FooterButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        RulesButton->setText(QApplication::translate("Lobby", "Regler", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ContactButton->setAccessibleName(QApplication::translate("Lobby", "FooterButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ContactButton->setText(QApplication::translate("Lobby", "Kontakt", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        AboutLabel->setAccessibleName(QApplication::translate("Lobby", "About", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        AboutLabel->setText(QApplication::translate("Lobby", "Kod och design av Fredrik Engstrand ", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Lobby: public Ui_Lobby {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOBBY_H
