/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QVBoxLayout *MainLayout;
    QFrame *HeaderFrame;
    QHBoxLayout *InfoBarHLayout;
    QLabel *TimeElapsed;
    QSpacerItem *horizontalSpacer;
    QLabel *Attempts;
    QVBoxLayout *GameVLayout;
    QVBoxLayout *AttemptHistoryVLayout;
    QSpacerItem *VSpacer;
    QFrame *AttemptFrame;
    QHBoxLayout *AttemptHlayout;
    QPushButton *ColorBox1;
    QPushButton *ColorBox2;
    QPushButton *ColorBox3;
    QPushButton *ColorBox4;
    QPushButton *ColorBoxFinished;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(585, 630);
        MainWindow->setMinimumSize(QSize(0, 0));
        MainWindow->setMaximumSize(QSize(585, 630));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        MainLayout = new QVBoxLayout();
        MainLayout->setSpacing(6);
        MainLayout->setObjectName(QStringLiteral("MainLayout"));
        HeaderFrame = new QFrame(centralWidget);
        HeaderFrame->setObjectName(QStringLiteral("HeaderFrame"));
        HeaderFrame->setStyleSheet(QStringLiteral(""));
        InfoBarHLayout = new QHBoxLayout(HeaderFrame);
        InfoBarHLayout->setSpacing(6);
        InfoBarHLayout->setContentsMargins(11, 11, 11, 11);
        InfoBarHLayout->setObjectName(QStringLiteral("InfoBarHLayout"));
        InfoBarHLayout->setContentsMargins(7, -1, 7, -1);
        TimeElapsed = new QLabel(HeaderFrame);
        TimeElapsed->setObjectName(QStringLiteral("TimeElapsed"));

        InfoBarHLayout->addWidget(TimeElapsed);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        InfoBarHLayout->addItem(horizontalSpacer);

        Attempts = new QLabel(HeaderFrame);
        Attempts->setObjectName(QStringLiteral("Attempts"));

        InfoBarHLayout->addWidget(Attempts);


        MainLayout->addWidget(HeaderFrame);

        GameVLayout = new QVBoxLayout();
        GameVLayout->setSpacing(6);
        GameVLayout->setObjectName(QStringLiteral("GameVLayout"));
        AttemptHistoryVLayout = new QVBoxLayout();
        AttemptHistoryVLayout->setSpacing(5);
        AttemptHistoryVLayout->setObjectName(QStringLiteral("AttemptHistoryVLayout"));
        AttemptHistoryVLayout->setSizeConstraint(QLayout::SetMaximumSize);
        AttemptHistoryVLayout->setContentsMargins(30, -1, 30, -1);
        VSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        AttemptHistoryVLayout->addItem(VSpacer);


        GameVLayout->addLayout(AttemptHistoryVLayout);

        AttemptFrame = new QFrame(centralWidget);
        AttemptFrame->setObjectName(QStringLiteral("AttemptFrame"));
        AttemptHlayout = new QHBoxLayout(AttemptFrame);
        AttemptHlayout->setSpacing(6);
        AttemptHlayout->setContentsMargins(11, 11, 11, 11);
        AttemptHlayout->setObjectName(QStringLiteral("AttemptHlayout"));
        ColorBox1 = new QPushButton(AttemptFrame);
        ColorBox1->setObjectName(QStringLiteral("ColorBox1"));

        AttemptHlayout->addWidget(ColorBox1);

        ColorBox2 = new QPushButton(AttemptFrame);
        ColorBox2->setObjectName(QStringLiteral("ColorBox2"));

        AttemptHlayout->addWidget(ColorBox2);

        ColorBox3 = new QPushButton(AttemptFrame);
        ColorBox3->setObjectName(QStringLiteral("ColorBox3"));

        AttemptHlayout->addWidget(ColorBox3);

        ColorBox4 = new QPushButton(AttemptFrame);
        ColorBox4->setObjectName(QStringLiteral("ColorBox4"));

        AttemptHlayout->addWidget(ColorBox4);

        ColorBoxFinished = new QPushButton(AttemptFrame);
        ColorBoxFinished->setObjectName(QStringLiteral("ColorBoxFinished"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ColorBoxFinished->sizePolicy().hasHeightForWidth());
        ColorBoxFinished->setSizePolicy(sizePolicy);

        AttemptHlayout->addWidget(ColorBoxFinished);

        AttemptHlayout->setStretch(0, 1);
        AttemptHlayout->setStretch(1, 1);
        AttemptHlayout->setStretch(2, 1);
        AttemptHlayout->setStretch(3, 1);
        AttemptHlayout->setStretch(4, 1);

        GameVLayout->addWidget(AttemptFrame);


        MainLayout->addLayout(GameVLayout);


        gridLayout->addLayout(MainLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QString());
#ifndef QT_NO_ACCESSIBILITY
        MainWindow->setAccessibleName(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        centralWidget->setAccessibleName(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        HeaderFrame->setAccessibleName(QApplication::translate("MainWindow", "Header", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        TimeElapsed->setText(QApplication::translate("MainWindow", "00:00:00", Q_NULLPTR));
        Attempts->setText(QApplication::translate("MainWindow", "F\303\266rs\303\266k kvar: 10", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        AttemptFrame->setAccessibleName(QApplication::translate("MainWindow", "AttemptFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ColorBox1->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox1->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBox2->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox2->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBox3->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox3->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBox4->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox4->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBoxFinished->setAccessibleName(QApplication::translate("MainWindow", "ColorBoxFinished", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBoxFinished->setText(QApplication::translate("MainWindow", "Gissa", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
