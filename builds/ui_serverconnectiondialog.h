/********************************************************************************
** Form generated from reading UI file 'serverconnectiondialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVERCONNECTIONDIALOG_H
#define UI_SERVERCONNECTIONDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_ServerConnectionDialog
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QHBoxLayout *InputHLayout;
    QLabel *IPLabel;
    QLineEdit *Input;

    void setupUi(QDialog *ServerConnectionDialog)
    {
        if (ServerConnectionDialog->objectName().isEmpty())
            ServerConnectionDialog->setObjectName(QStringLiteral("ServerConnectionDialog"));
        ServerConnectionDialog->resize(305, 107);
        gridLayout = new QGridLayout(ServerConnectionDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        buttonBox = new QDialogButtonBox(ServerConnectionDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);

        InputHLayout = new QHBoxLayout();
        InputHLayout->setObjectName(QStringLiteral("InputHLayout"));
        IPLabel = new QLabel(ServerConnectionDialog);
        IPLabel->setObjectName(QStringLiteral("IPLabel"));

        InputHLayout->addWidget(IPLabel);

        Input = new QLineEdit(ServerConnectionDialog);
        Input->setObjectName(QStringLiteral("Input"));

        InputHLayout->addWidget(Input);


        gridLayout->addLayout(InputHLayout, 0, 0, 1, 1);


        retranslateUi(ServerConnectionDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ServerConnectionDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ServerConnectionDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ServerConnectionDialog);
    } // setupUi

    void retranslateUi(QDialog *ServerConnectionDialog)
    {
        ServerConnectionDialog->setWindowTitle(QApplication::translate("ServerConnectionDialog", "Anslut", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ServerConnectionDialog->setAccessibleName(QApplication::translate("ServerConnectionDialog", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        IPLabel->setText(QApplication::translate("ServerConnectionDialog", "IP:", Q_NULLPTR));
        Input->setPlaceholderText(QApplication::translate("ServerConnectionDialog", "Ex 127.0.0.1", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ServerConnectionDialog: public Ui_ServerConnectionDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVERCONNECTIONDIALOG_H
