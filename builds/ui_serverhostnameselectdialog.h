/********************************************************************************
** Form generated from reading UI file 'serverhostnameselectdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVERHOSTNAMESELECTDIALOG_H
#define UI_SERVERHOSTNAMESELECTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ServerHostnameSelectDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *MainVLayout;
    QLabel *IPSelectLabel;
    QComboBox *IPComboSelect;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ServerHostnameSelectDialog)
    {
        if (ServerHostnameSelectDialog->objectName().isEmpty())
            ServerHostnameSelectDialog->setObjectName(QStringLiteral("ServerHostnameSelectDialog"));
        ServerHostnameSelectDialog->resize(342, 116);
        gridLayout = new QGridLayout(ServerHostnameSelectDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        MainVLayout = new QVBoxLayout();
        MainVLayout->setObjectName(QStringLiteral("MainVLayout"));
        IPSelectLabel = new QLabel(ServerHostnameSelectDialog);
        IPSelectLabel->setObjectName(QStringLiteral("IPSelectLabel"));
        IPSelectLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        MainVLayout->addWidget(IPSelectLabel);

        IPComboSelect = new QComboBox(ServerHostnameSelectDialog);
        IPComboSelect->setObjectName(QStringLiteral("IPComboSelect"));

        MainVLayout->addWidget(IPComboSelect);


        gridLayout->addLayout(MainVLayout, 0, 0, 1, 1);

        buttonBox = new QDialogButtonBox(ServerHostnameSelectDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);


        retranslateUi(ServerHostnameSelectDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ServerHostnameSelectDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ServerHostnameSelectDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ServerHostnameSelectDialog);
    } // setupUi

    void retranslateUi(QDialog *ServerHostnameSelectDialog)
    {
        ServerHostnameSelectDialog->setWindowTitle(QApplication::translate("ServerHostnameSelectDialog", "Dialog", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ServerHostnameSelectDialog->setAccessibleName(QApplication::translate("ServerHostnameSelectDialog", "MainWindow", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        IPSelectLabel->setText(QApplication::translate("ServerHostnameSelectDialog", "V\303\244lj vilken lokal IP-address du vill starta servern ifr\303\245n:\n"
"Addresserna \303\244r olika beroende p\303\245 om du k\303\266r tr\303\245dl\303\266st eller via sladd.\n"
"Det kommer inte funka om du startar servern fr\303\245n fel address! ", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class ServerHostnameSelectDialog: public Ui_ServerHostnameSelectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVERHOSTNAMESELECTDIALOG_H
